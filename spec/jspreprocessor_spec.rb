require 'spec_helper'

describe JSPreprocessor do
  before(:all) do
    @pre = JSPreprocessor.new 'development'
  end

  it 'should process files' do #huehue
    processed_file = @pre.process "#{Dir.pwd}/example_js/", 'main.js'
    File.open 'processed.js', 'w' do |file|
      file.write processed_file
    end
  end
end