# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "jspreprocessor"
  spec.version       = '0.0.6'
  spec.authors       = ["Caleb Albritton"]
  spec.email         = ["ithinkincode@gmail.com"]
  spec.summary       = %q{Like Browserify but the C way.}
  spec.description   = %q{Include JavaScript files directly into your source code using #include 'somefile'}
  spec.homepage      = "https://github.com/C0deMaver1ck/JSPreprocessor"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.6'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec', '~> 2.14'
  spec.add_development_dependency 'pry'
end
