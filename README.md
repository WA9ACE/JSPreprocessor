# JSPreprocessor

Like Browserify but the C way.
Include JavaScript files directly into your source code using #include 'somefile'
Include files installed via bower with inclusion guards: #include <undsercore>
Use #define FOO bar to replace every instance of FOO with bar.

## Installation

Add this line to your application's Gemfile:

    gem 'jspreprocessor'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jspreprocessor

## Gem Usage

	@pre = JSPreprocessor.new
	processed_file = @pre.process './folder_with_all_your_javascript/', 'entry_point.js'

	File.open 'processed.js', 'w' do |file|
      file.write processed_file
    end

## JavaScript Usage

	#include <underscore> // This will only work if you've installed underscore via bower
	#include 'lib/stuff'

	#define FOO bar

	var s = new Stuff();
	s.doStuff();

## Contributing

1. Fork it ( https://github.com/C0deMaver1ck/JSPreprocessor/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
