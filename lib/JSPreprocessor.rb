require 'json'

class JSPreprocessor

  class MissingDependencyException < Exception
  end

  def initialize(env)
    @env = env
    @defines = []
  end

  def process(cwd, main_file)
    main = File.open(cwd + main_file).read.split "\n"
    compiled_file = recurse_files(cwd, main).join "\n"
    replace_defines compiled_file
  end

  def recurse_files(cwd, file)
    file.each_with_index do |line, index|
      if line.include? '#include'
        if line.include? '<' #inclusion guards, use bower
          if File.file? '.bowerrc' # check if they keep the bower files somewhere else
            bowerrc = JSON.parse File.open('.bowerrc').read
            if !bowerrc['directory'].nil? # if a custom directory has been set
              @bower_dir = bowerrc['directory']
            else
              @bower_dir = 'bower_components'
            end
          else
            @bower_dir = 'bower_components'
          end

          package = line.match(/<(.*)>/)[1]
          bower_json = "#{@bower_dir}/#{package}/bower.json"
          package_json = "#{@bower_dir}/#{package}/package.json"

          if File.exists? bower_json
            json = File.open(bower_json).read
          elsif File.exists? package_json
            json = File.open(package_json).read
          else
            raise MissingDependencyException, "bower.json, package.json is missing, or #{package} does not exist."
          end

          package_path = JSON.parse(json)['main']

          if package_path.class == Array
            package_path.each_with_index do |package, index|
              if package[-2..-1] == 'js'
                package_path = package
                break
              end
            end
          end

          library = File.open "#{@bower_dir}/#{package}/#{package_path}"
          
          if @env == 'development'
            filestamp = "// #{File.basename(library)} at #{Time.now}\n"
            library = library.read
            library.prepend filestamp
          else
            library = library.read
          end

          file[index] = library
        else # this is not an inclusion guard, so it's relative
          to_include = cwd + line.match(/('|")(.*)('|")/)[2] + '.js'
          next_wd = Pathname.new(to_include).dirname.to_s + '/'
          next_file = File.open(to_include).read.split("\n")

          if @env == 'development'
            filestamp = "// #{to_include} at #{Time.now}"
            next_file.unshift filestamp
          end

          file[index] = recurse_files next_wd, next_file
        end
      elsif line.include? '#define'
        @defines << line[8..-1].split(' ')
        file.delete_at index
      end
    end
  end

  def replace_defines(file)
    @defines.each do |definition|
      file.gsub! definition[0], definition[1]
    end

    file
  end
end